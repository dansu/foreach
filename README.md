# Foreach

Sublime Text 3 plugin - foreach lambda expression evaluator

Run python lambda on each selection with selected text as input

## Usage


Highlight areas:
	Ctrl + Alt + l for Windows
	Type wanted expression and press Enter