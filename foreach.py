import sublime, sublime_plugin


PREFIX = "lambda s, idx, ln:"
class ForeachCommand(sublime_plugin.TextCommand):
    '''Run a lambda on each selection, inputs to lambda:
- (s) selected text
- (idx) 0-based selection index
- (ln) 0-based line number of the start of each selection
    '''

    def run(self, edit, user_input=None):
        if user_input == None:
            sublime.active_window().show_input_panel("foreach| {0}".format(PREFIX), "s", self.on_done, None, None)
        else:
            fnc = eval(PREFIX+str(user_input))
            view = self.view
            for idx, region in enumerate(view.sel()):  
                if not region.empty():  
                    # Get the selected text  
                    s = view.substr(region)  
                    # Get linenumber
                    ln = view.rowcol(region.begin())[0]
                    # Run lambda on selection
                    s = fnc(s, idx, ln)
                    # Replace the selection with transformed text
                    view.replace(edit, region, str(s)) 

    def on_done(self, text):
        self.view.run_command("foreach",{"user_input": text})
